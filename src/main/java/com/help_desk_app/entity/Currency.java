package com.help_desk_app.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class Currency {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonProperty("Cur_ID")
    private Long currencyId;

    @JsonProperty("Cur_ParentID")
    private Long parentID;

    @JsonProperty("Cur_Code")
    private String code;

    @JsonProperty("Cur_Abbreviation")
    private String abbreviation;

    @JsonProperty("Cur_Name")
    private String name;

    @JsonProperty("Cur_Name_Bel")
    private String nameBel;

    @JsonProperty("Cur_Name_Eng")
    private String nameEng;

    @JsonProperty("Cur_QuotName")
    private String quotName;

    @JsonProperty("Cur_QuotName_Bel")
    private String quotNameBel;

    @JsonProperty("Cur_QuotName_Eng")
    private String quotNameEng;

    @JsonProperty("Cur_NameMulti")
    private String nameMulti;

    @JsonProperty("Cur_Name_BelMulti")
    private String nameBelMulti;

    @JsonProperty("Cur_Name_EngMulti")
    private String nameEngMulti;

    @JsonProperty("Cur_Scale")
    private Integer scale;

    @JsonProperty("Cur_Periodicity")
    private Integer periodicity;

    @JsonProperty("Cur_DateStart")
    private Date startDate;

    @JsonProperty("Cur_DateEnd")
    private Date endDate;

    public Currency() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(Long currencyId) {
        this.currencyId = currencyId;
    }

    public Long getParentID() {
        return parentID;
    }

    public void setParentID(Long parentID) {
        this.parentID = parentID;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameBel() {
        return nameBel;
    }

    public void setNameBel(String nameBel) {
        this.nameBel = nameBel;
    }

    public String getNameEng() {
        return nameEng;
    }

    public void setNameEng(String nameEng) {
        this.nameEng = nameEng;
    }

    public String getQuotName() {
        return quotName;
    }

    public void setQuotName(String quotName) {
        this.quotName = quotName;
    }

    public String getQuotNameBel() {
        return quotNameBel;
    }

    public void setQuotNameBel(String quotNameBel) {
        this.quotNameBel = quotNameBel;
    }

    public String getQuotNameEng() {
        return quotNameEng;
    }

    public void setQuotNameEng(String quotNameEng) {
        this.quotNameEng = quotNameEng;
    }

    public String getNameMulti() {
        return nameMulti;
    }

    public void setNameMulti(String nameMulti) {
        this.nameMulti = nameMulti;
    }

    public String getNameBelMulti() {
        return nameBelMulti;
    }

    public void setNameBelMulti(String nameBelMulti) {
        this.nameBelMulti = nameBelMulti;
    }

    public String getNameEngMulti() {
        return nameEngMulti;
    }

    public void setNameEngMulti(String nameEngMulti) {
        this.nameEngMulti = nameEngMulti;
    }

    public Integer getScale() {
        return scale;
    }

    public void setScale(Integer scale) {
        this.scale = scale;
    }

    public Integer getPeriodicity() {
        return periodicity;
    }

    public void setPeriodicity(Integer periodicity) {
        this.periodicity = periodicity;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Currency currency = (Currency) o;
        return Objects.equals(id, currency.id) &&
                Objects.equals(currencyId, currency.currencyId) &&
                Objects.equals(parentID, currency.parentID) &&
                Objects.equals(code, currency.code) &&
                Objects.equals(abbreviation, currency.abbreviation) &&
                Objects.equals(name, currency.name) &&
                Objects.equals(nameBel, currency.nameBel) &&
                Objects.equals(nameEng, currency.nameEng) &&
                Objects.equals(quotName, currency.quotName) &&
                Objects.equals(quotNameBel, currency.quotNameBel) &&
                Objects.equals(quotNameEng, currency.quotNameEng) &&
                Objects.equals(nameMulti, currency.nameMulti) &&
                Objects.equals(nameBelMulti, currency.nameBelMulti) &&
                Objects.equals(nameEngMulti, currency.nameEngMulti) &&
                Objects.equals(scale, currency.scale) &&
                Objects.equals(periodicity, currency.periodicity) &&
                Objects.equals(startDate, currency.startDate) &&
                Objects.equals(endDate, currency.endDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, currencyId, parentID, code, abbreviation, name, nameBel, nameEng, quotName, quotNameBel, quotNameEng, nameMulti, nameBelMulti, nameEngMulti, scale, periodicity, startDate, endDate);
    }

    @Override
    public String toString() {
        return "Currency{" +
                "id=" + id +
                ", currencyId=" + currencyId +
                ", parentID=" + parentID +
                ", code='" + code + '\'' +
                ", abbreviation='" + abbreviation + '\'' +
                ", name='" + name + '\'' +
                ", nameBel='" + nameBel + '\'' +
                ", nameEng='" + nameEng + '\'' +
                ", quotName='" + quotName + '\'' +
                ", quotNameBel='" + quotNameBel + '\'' +
                ", quotNameEng='" + quotNameEng + '\'' +
                ", nameMulti='" + nameMulti + '\'' +
                ", nameBelMulti='" + nameBelMulti + '\'' +
                ", nameEngMulti='" + nameEngMulti + '\'' +
                ", scale=" + scale +
                ", periodicity=" + periodicity +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                '}';
    }
}