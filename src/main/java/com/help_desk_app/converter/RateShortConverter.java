package com.help_desk_app.converter;

import com.help_desk_app.dto.RateShortDto;
import com.help_desk_app.pojo.RateShort;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class RateShortConverter {
    private Logger logger = Logger.getLogger(this.getClass());

    public RateShortDto toDto(RateShort rateShort) {
        logger.debug(String.format("RateShortDto from %s", rateShort));
        RateShortDto dto = new RateShortDto(rateShort.getDate(),rateShort.getRate());
        logger.info(String.format("RateShortDto was created : %s", dto));
        return dto;
    }
}
