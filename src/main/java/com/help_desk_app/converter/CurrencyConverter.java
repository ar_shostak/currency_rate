package com.help_desk_app.converter;

import com.help_desk_app.dto.CurrencyDto;
import com.help_desk_app.entity.Currency;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class CurrencyConverter {
    private Logger logger = Logger.getLogger(this.getClass());

    public CurrencyDto toDto(Currency currency) {
        logger.debug(String.format("Creating CurrencyDto from %s", currency));
        CurrencyDto dto = new CurrencyDto(currency.getNameEng(), currency.getAbbreviation(),
                currency.getCurrencyId(), currency.getParentID());
        logger.info(String.format("CurrencyDto was created : %s", dto));
        return dto;
    }
}
