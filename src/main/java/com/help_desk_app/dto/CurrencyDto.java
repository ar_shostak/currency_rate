package com.help_desk_app.dto;

import java.util.Objects;

public class CurrencyDto {
    private Long id;
    private Long parentId;
    private String nameEng;
    private String abbreviation;

    public CurrencyDto(String nameEng, String abbreviation, Long id, Long parentId) {
        this.id = id;
        this.parentId = parentId;
        this.nameEng = nameEng;
        this.abbreviation = abbreviation;
    }

    public String getNameEng() {
        return nameEng;
    }

    public void setNameEng(String nameEng) {
        this.nameEng = nameEng;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CurrencyDto that = (CurrencyDto) o;
        return Objects.equals(nameEng, that.nameEng) &&
                Objects.equals(abbreviation, that.abbreviation) &&
                Objects.equals(id, that.id) &&
                Objects.equals(parentId, that.parentId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nameEng, abbreviation, id, parentId);
    }

    @Override
    public String toString() {
        return "CurrencyDto{" +
                "id=" + id +
                ", parentId=" + parentId +
                ", nameEng='" + nameEng + '\'' +
                ", abbreviation='" + abbreviation + '\'' +
                '}';
    }
}
