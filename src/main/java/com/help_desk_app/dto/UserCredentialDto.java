package com.help_desk_app.dto;

import javax.validation.constraints.*;
import java.util.Objects;

public class UserCredentialDto {
    private static final String EMAIL_PATTERN = "^.+@.*\\..+$";
    private static final String PASSWORD_PATTERN =  "^(?=.*?\\p{Lu})(?=.*?\\p{Ll})(?=.*?\\d)" +
            "(?=.*?[a-zA-Z0-9~.\"(),:;<>@\\[\\]!#$%&'*+\\-/=?^_`{|}]).*$";
    private final static String INVALID_CREDENTIALS = "{dto.UserCredentialDto.message}";

    @NotNull
    @Size(min  = 4, max = 100, message = INVALID_CREDENTIALS)
    @Pattern(regexp = EMAIL_PATTERN, message = INVALID_CREDENTIALS)
    private String email;

    @Size(min = 6, max = 20, message = INVALID_CREDENTIALS)
    @Pattern(regexp = PASSWORD_PATTERN, message = INVALID_CREDENTIALS)
    private String password;

    public UserCredentialDto(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public UserCredentialDto() {}

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserCredentialDto userDto = (UserCredentialDto) o;
        return Objects.equals(email, userDto.email) &&
                Objects.equals(password, userDto.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email, password);
    }

    @Override
    public String toString() {
        return "UserCredentialDto{" +
                ", email='" + email + '\'' +
                ", password='" + "******" + '\'' +
                '}';
    }
}
