package com.help_desk_app.dto;

import java.util.Date;
import java.util.Objects;

public class RateShortDto {
    private Date date;
    private Double rate;

    public RateShortDto(Date date, Double rate) {
        this.date = date;
        this.rate = rate;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RateShortDto that = (RateShortDto) o;
        return Objects.equals(date, that.date) &&
                Objects.equals(rate, that.rate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(date, rate);
    }

    @Override
    public String toString() {
        return "RateShortDto{" +
                "date=" + date +
                ", rate=" + rate +
                '}';
    }
}
