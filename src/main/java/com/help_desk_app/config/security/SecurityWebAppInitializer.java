package com.help_desk_app.config.security;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

public class SecurityWebAppInitializer extends
        AbstractSecurityWebApplicationInitializer {
}