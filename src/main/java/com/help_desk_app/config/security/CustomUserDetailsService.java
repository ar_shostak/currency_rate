package com.help_desk_app.config.security;

import com.help_desk_app.dao.impl.UserDaoImpl;
import com.help_desk_app.entity.User;
import com.help_desk_app.exception.UserNameNotFoundException;
import org.apache.log4j.Logger;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

@Service
public class CustomUserDetailsService implements UserDetailsService{
    private Logger logger = Logger.getLogger(this.getClass());
    private final UserDaoImpl dao;

    public CustomUserDetailsService(UserDaoImpl dao){
        this.dao=dao;
    }

    @Override
    public CustomUserPrincipal loadUserByUsername(final String email) {
        logger.debug(String.format("Trying to create CustomUserPrincipal for email : %s", email));
        final User user = dao.getUserByEmail(email)
                        .orElseThrow(() -> new UserNameNotFoundException("User with email = " + email + " not found"));
        CustomUserPrincipal principal = new CustomUserPrincipal(user);
        logger.info(String.format("CustomUserPrincipal was created : %s", principal));
        return principal;
    }
}
