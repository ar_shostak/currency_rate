package com.help_desk_app.config.web;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.*;

@Configuration
@EnableWebMvc
@EnableGlobalMethodSecurity(prePostEnabled=true)
@ComponentScan("com.help_desk_app")
public class WebConfig implements WebMvcConfigurer {
    private static final String MESSAGE_PROPERTIES = "classpath:messages";
    private static final String ENCODING = "UTF-8";
    private static final String PATH_MAPPING = "/**";
    private static final String ALLOWED_HEADERS = "*";
    private static final String ALLOWED_ORIGIN= "http://localhost:3000";
    private static final String[] ALLOWED_METHODS = new String[]{"GET", "POST", "PUT", "OPTIONS", "DELETE"};

     @Override
     public void addCorsMappings(CorsRegistry registry) {
          registry.addMapping(PATH_MAPPING)
                  .allowedMethods(ALLOWED_METHODS)
                  .allowedHeaders(ALLOWED_HEADERS)
                  .allowedOrigins(ALLOWED_ORIGIN);
     }

    @Bean
    public MessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasename(MESSAGE_PROPERTIES);
        messageSource.setDefaultEncoding(ENCODING);
        return messageSource;
    }

    @Bean
    public RestTemplate restTemplate() {
         RestTemplate restTemplate = new RestTemplate();
         restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
         return restTemplate;
    }
}

