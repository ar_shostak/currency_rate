package com.help_desk_app.config.hibernate;

import org.apache.tomcat.dbcp.dbcp2.BasicDataSource;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@PropertySource("classpath:hibernate.properties")
@EnableTransactionManagement
public class HibernateUtils {
    private final static String URL = "jdbc.url";
    private final static String DB_DRIVER = "jdbc.driverClassName";
    private static final String CREATE_STRATEGY = "hibernate.hbm2ddl.auto";
    private final static String DIALECT = "hibernate.dialect";
    private final static String SHOW_SQL = "hibernate.show_sql";
    private final static String USER_NAME = "jdbc.username";
    private final static String PASSWORD = "jdbc.password";
    private final static String PACKAGE = "com.help_desk_app";
    private final static String TIME_ZONE = "hibernate.jdbc.time_zone";

    private final Environment environment;

    public HibernateUtils(Environment environment) {
        this.environment = environment;
    }

    @Bean
    public LocalSessionFactoryBean sessionFactory() {
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(dataSource());
        sessionFactory.setPackagesToScan(PACKAGE);
        sessionFactory.setHibernateProperties(hibernateProperties());
        return sessionFactory;
    }

    @Bean
    public DataSource dataSource() {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName(environment.getRequiredProperty(DB_DRIVER));
        dataSource.setUrl(environment.getRequiredProperty(URL));
        dataSource.setUsername(environment.getRequiredProperty(USER_NAME));
        dataSource.setPassword(environment.getRequiredProperty(PASSWORD));
        return dataSource;
    }

    @Bean
    public HibernateTransactionManager transactionManager(SessionFactory sessionFactory) {
        HibernateTransactionManager transactionManager = new HibernateTransactionManager();
        transactionManager.setSessionFactory(sessionFactory);
        return transactionManager;
    }

    private Properties hibernateProperties() {
        Properties properties = new Properties();
        properties.put(DIALECT, environment.getRequiredProperty(DIALECT));
        properties.put(SHOW_SQL, environment.getRequiredProperty(SHOW_SQL));
        properties.put(TIME_ZONE, environment.getRequiredProperty(TIME_ZONE));
        properties.put(CREATE_STRATEGY, environment.getRequiredProperty(CREATE_STRATEGY));
        return properties;
    }
}
