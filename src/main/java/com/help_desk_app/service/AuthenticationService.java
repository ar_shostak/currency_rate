package com.help_desk_app.service;

import com.help_desk_app.config.security.CustomUserDetailsService;
import com.help_desk_app.config.security.CustomUserPrincipal;
import com.help_desk_app.converter.UserConverter;
import com.help_desk_app.dto.UserCredentialDto;
import com.help_desk_app.dto.UserDto;
import com.help_desk_app.entity.User;
import com.help_desk_app.exception.AuthException;
import com.help_desk_app.utils.JwtUtils;
import org.apache.log4j.Logger;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class AuthenticationService {
    Logger logger = Logger.getLogger(this.getClass());

    private final UserConverter userConverter;
    private final JwtUtils jwtUtils;
    private final DaoAuthenticationProvider provider;
    private final CustomUserDetailsService customUserDetailsService;

    public AuthenticationService(UserConverter userConverter,
                                    DaoAuthenticationProvider provider,
                                    CustomUserDetailsService customUserDetailsService,
                                    JwtUtils jwtUtils) {
        this.customUserDetailsService = customUserDetailsService;
        this.userConverter = userConverter;
        this.provider = provider;
        this.jwtUtils = jwtUtils;
    }

    public User getAuthenticateUser() {
        return  ((CustomUserPrincipal) SecurityContextHolder.getContext().getAuthentication()
            .getPrincipal()).getUser();
    }

    public UserDto authenticate(UserCredentialDto credentialDto) {
        logger.debug(String.format("Try to authenticate user with credentials %s", credentialDto));
        try {
            provider.authenticate(new UsernamePasswordAuthenticationToken(credentialDto.getEmail(), credentialDto.getPassword()));
        } catch (BadCredentialsException e) {
            logger.error(String.format("Incorrect credentials : %s", credentialDto), e);
            throw new AuthException("Incorrect credentials");
        }
        String token = jwtUtils.createToken(credentialDto.getEmail());
        User user = (customUserDetailsService.loadUserByUsername(credentialDto.getEmail())).getUser();
        UserDto dto = userConverter.toDto(user);
        dto.setJwt(token);
        logger.info(String.format("The user %s was authenticated ", credentialDto));
        return dto;
    }
}
