package com.help_desk_app.service;

import com.help_desk_app.client.CurrencyClient;
import com.help_desk_app.converter.CurrencyConverter;
import com.help_desk_app.dao.CurrencyDao;
import com.help_desk_app.dto.CurrencyDto;
import com.help_desk_app.entity.Currency;
import com.help_desk_app.exception.NotFoundException;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional(readOnly = true)
public class CurrencyService {
    private static boolean isCurrenciesDownloaded = false;
    private Logger logger = Logger.getLogger(this.getClass());
    private final CurrencyClient currencyClient;
    private final CurrencyDao currencyDao;
    private final CurrencyConverter currencyConverter;

    public CurrencyService(CurrencyClient currencyClient, CurrencyDao currencyDao, CurrencyConverter currencyConverter) {
        this.currencyClient = currencyClient;
        this.currencyDao = currencyDao;
        this.currencyConverter = currencyConverter;
    }

    public Set<CurrencyDto> getAll() {
        logger.debug("Trying to get all currencies");
        if (isCurrenciesDownloaded == false) {
            setCurrencies();
        }
        return currencyDao.getAll()
            .stream()
            .map(currencyConverter::toDto)
            .collect(Collectors.toSet());
    }

    @Transactional
    private void setCurrencies() {
        logger.debug("Trying to download all currencies");
        currencyClient.getCurrencies().stream().forEach(currencyDao::create);
        isCurrenciesDownloaded = true;
        logger.debug("Currencies was downloaded");
    }

    public Currency getOneByCurrencyId(Long id) {
        logger.debug(String.format("Trying to get Currency with id %s", id));
        return currencyDao.getOneByCurrencyId(id).orElseThrow(() -> new NotFoundException(Currency.class, id));
    }
}
