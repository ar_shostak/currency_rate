package com.help_desk_app.service;

import com.help_desk_app.converter.UserConverter;
import com.help_desk_app.dao.UserDao;
import com.help_desk_app.dto.UserDto;
import com.help_desk_app.entity.User;
import com.help_desk_app.exception.NotFoundException;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class UserService {
    private Logger logger = Logger.getLogger(this.getClass());

    private final UserDao userDao;
    private final UserConverter userConverter;

    public UserService(UserDao userDao, UserConverter userConverter) {
        this.userDao = userDao;
        this.userConverter = userConverter;
    }

    public UserDto getUserById(Long id) {
        logger.debug(String.format("Try to find User with id %s", id));
        User user = userDao.getOne(id).orElseThrow(() -> new NotFoundException(User.class, id));
        return userConverter.toDto(user);
    }

    public UserDto getUser(String email) {
        logger.debug(String.format("Try to find User with email %s", email));
        User user = userDao.getUserByEmail(email).orElseThrow(() -> new NotFoundException(User.class, email));
        return userConverter.toDto(user);
    }
}
