package com.help_desk_app.service;

import com.help_desk_app.client.RateShortClient;
import com.help_desk_app.converter.RateShortConverter;
import com.help_desk_app.dto.RateShortDto;
import com.help_desk_app.entity.Currency;
import com.help_desk_app.pojo.RateDynamic;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional(readOnly = true)
public class RateShortService {
    private Logger logger = Logger.getLogger(this.getClass());
    private final RateShortClient rateShortClient;
    private final RateShortConverter rateShortConverter;
    private final CurrencyService currencyService;

    public RateShortService(RateShortClient rateShortClient, RateShortConverter rateShortConverter, CurrencyService currencyService) {
        this.rateShortClient = rateShortClient;
        this.rateShortConverter = rateShortConverter;
        this.currencyService = currencyService;
    }

    public RateDynamic get(Long id, Date startDate, Date enddate) {
        logger.debug(String.format("Trying to get RateShorts for currency with id %s", id));
        Set<RateShortDto> rateShorts = new HashSet<>();
        Currency currentCurrency = currencyService.getOneByCurrencyId(id);
        Currency parent = getParent(currentCurrency, enddate);

        while(parent != null) {
           logger.debug(String.format("Trying to download RateShorts for currency with id %s", id));
           rateShorts.addAll(rateShortClient.getById(currentCurrency.getCurrencyId(), startDate, enddate).stream().map(rateShortConverter::toDto).collect(Collectors.toSet()));
           currentCurrency = parent;
           parent = getParent(currentCurrency, enddate);
        }
        logger.debug(String.format("Trying to download RateShorts for currency with id %s", id));
        rateShorts.addAll(rateShortClient.getById(currentCurrency.getCurrencyId(), startDate, enddate).stream().map(rateShortConverter::toDto).collect(Collectors.toSet()));
        RateDynamic rateDynamic = new RateDynamic(currentCurrency.getNameEng(), currentCurrency.getAbbreviation(), currentCurrency.getScale());
        for(RateShortDto rateShort : rateShorts) {
            rateDynamic.getRateShortDtos().add(rateShort);
        }
        return rateDynamic;
    }

    private Currency getParent(Currency currentCurrency, Date enddate) {
        if (currentCurrency.getEndDate().before(enddate) && (!currentCurrency.getCurrencyId().equals(currentCurrency.getParentID()))) {
            return currencyService.getOneByCurrencyId(currentCurrency.getParentID());
        } return null;
    }
}
