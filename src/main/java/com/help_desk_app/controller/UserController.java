package com.help_desk_app.controller;

import com.help_desk_app.dto.UserDto;
import com.help_desk_app.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserDto> getUser(@PathVariable final Long id) {
        return ResponseEntity.ok(userService.getUserById(id));
    }
}
