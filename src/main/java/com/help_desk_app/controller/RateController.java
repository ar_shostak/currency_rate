package com.help_desk_app.controller;

import com.help_desk_app.pojo.RateDynamic;
import com.help_desk_app.service.RateShortService;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@RequestMapping("/rates")
@RestController
public class RateController {
    private static final String PATTERN = "dd/MM/yyyy";
    private final RateShortService rateShortService;

    public RateController(RateShortService rateShortService) {
        this.rateShortService = rateShortService;
    }

    @GetMapping(value = "/dynamics/{id}")
    public ResponseEntity<RateDynamic> get(HttpServletRequest request, @PathVariable final Long id,
                                           @RequestParam("startdate") @DateTimeFormat(pattern = PATTERN) final Date startdate,
                                           @RequestParam("enddate") @DateTimeFormat(pattern = PATTERN) final Date enddate) {
        RateDynamic rateDynamic = rateShortService.get(id, startdate, enddate);
        return ResponseEntity.ok(rateDynamic);
    }
}
