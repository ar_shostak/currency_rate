package com.help_desk_app.controller;

import com.help_desk_app.dto.UserCredentialDto;
import com.help_desk_app.dto.UserDto;
import com.help_desk_app.service.AuthenticationService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

@RestController
@RequestMapping("/")
public class AuthenticationController {

    private final AuthenticationService authService;

    public AuthenticationController(AuthenticationService authService) {
        this.authService = authService;
    }

    @PostMapping(value = "/login")
    public ResponseEntity<UserDto> login(@Valid @RequestBody final UserCredentialDto credentialDto) {
        return ResponseEntity.ok(authService.authenticate(credentialDto));
    }

    @GetMapping(value = "/logoutSuccessfull")
    public ResponseEntity logout() {
        SecurityContextHolder.clearContext();
        return ResponseEntity.ok().build();
    }
}