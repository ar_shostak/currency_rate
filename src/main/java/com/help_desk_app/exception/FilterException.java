package com.help_desk_app.exception;

public class FilterException extends RuntimeException{
    public FilterException(String exceptionText) {super(exceptionText);}
}
