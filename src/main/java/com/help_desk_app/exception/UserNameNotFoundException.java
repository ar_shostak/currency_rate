package com.help_desk_app.exception;

public class UserNameNotFoundException extends RuntimeException {
        public UserNameNotFoundException(String str){
            super(str);
        }
    }

