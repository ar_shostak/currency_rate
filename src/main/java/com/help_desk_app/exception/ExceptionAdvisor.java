package com.help_desk_app.exception;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.net.ConnectException;
import java.net.SocketTimeoutException;

@RestControllerAdvice
public class ExceptionAdvisor extends ResponseEntityExceptionHandler {
    private Logger logger = Logger.getLogger(this.getClass());

    @ExceptionHandler({NotFoundException.class, UserNameNotFoundException.class, FilterException.class})
    public ResponseEntity<Object> handleNotFound(Exception ex) {
        logger.error(ex.getMessage());
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({AuthException.class, IllegalArgumentException.class})
    public ResponseEntity<Object> handleAuthException(Exception ex) {
        logger.error(ex.getMessage());
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({SocketTimeoutException.class, ConnectException.class})
    public ResponseEntity<Object> handleConnectionException(Exception ex) {
        logger.error(ex.getMessage());
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_GATEWAY);
    }
}
