package com.help_desk_app.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;
import java.util.Objects;

@Entity
public class RateShort {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonProperty("Cur_ID")
    private Long currencyId;

    @JsonProperty("Date")
    private Date date;

    @JsonProperty("Cur_OfficialRate")
    private Double rate;

    public RateShort() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(Long currencyId) {
        this.currencyId = currencyId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RateShort rateShort = (RateShort) o;
        return Objects.equals(id, rateShort.id) &&
                Objects.equals(currencyId, rateShort.currencyId) &&
                Objects.equals(date, rateShort.date) &&
                Objects.equals(rate, rateShort.rate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, currencyId, date, rate);
    }

    @Override
    public String toString() {
        return "RateShort{" +
                "id=" + id +
                ", currencyId=" + currencyId +
                ", date=" + date +
                ", rate=" + rate +
                '}';
    }
}
