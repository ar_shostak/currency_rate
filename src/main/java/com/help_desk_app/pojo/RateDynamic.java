package com.help_desk_app.pojo;

import com.help_desk_app.dto.RateShortDto;
import java.util.*;

public class RateDynamic {
    private String currencyName;
    private String abbreviation;
    private Integer scale;
    private List<RateShortDto> rateShortDtos = new ArrayList<>();

    public RateDynamic(String currencyName, String abbreviation, Integer scale) {
        this.currencyName = currencyName;
        this.abbreviation = abbreviation;
        this.scale = scale;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public Integer getScale() {
        return scale;
    }

    public void setScale(Integer scale) {
        this.scale = scale;
    }

    public List<RateShortDto> getRateShortDtos() {
        return rateShortDtos;
    }

    public void setRateShortDtos(List<RateShortDto> rateShortDtos) {
        this.rateShortDtos = rateShortDtos;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RateDynamic that = (RateDynamic) o;
        return Objects.equals(currencyName, that.currencyName) &&
                Objects.equals(abbreviation, that.abbreviation) &&
                Objects.equals(scale, that.scale) &&
                Objects.equals(rateShortDtos, that.rateShortDtos);
    }

    @Override
    public int hashCode() {
        return Objects.hash(currencyName, abbreviation, scale, rateShortDtos);
    }

    @Override
    public String toString() {
        return "RateDynamic{" +
                "currencyName='" + currencyName + '\'' +
                ", abbreviation='" + abbreviation + '\'' +
                ", scale=" + scale +
                ", rateShortDtos=" + rateShortDtos +
                '}';
    }
}