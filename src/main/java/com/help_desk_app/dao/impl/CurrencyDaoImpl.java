package com.help_desk_app.dao.impl;

import com.help_desk_app.dao.CurrencyDao;
import com.help_desk_app.entity.Currency;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import java.util.Optional;

@Repository
public class CurrencyDaoImpl extends BaseDaoImpl<Currency> implements CurrencyDao {
    private static final String GET_ONE_BY_CURRENCY_ID = "from Currency where currencyId = :currencyId";
    private static final String CURRENCY_ID_COLUMN = "currencyId";

    private Logger logger = Logger.getLogger(this.getClass());

    public CurrencyDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public Optional<Currency> getOneByCurrencyId(Long currencyId) {
        logger.debug(String.format("Trying to find Currency with id = %s", currencyId));
        Optional<Currency> currency =  (getSession().createQuery(GET_ONE_BY_CURRENCY_ID, Currency.class)
               .setParameter(CURRENCY_ID_COLUMN, currencyId).uniqueResultOptional());
        logger.info(String.format("Currency were found  %s", currency));
        return currency;
    }

}
