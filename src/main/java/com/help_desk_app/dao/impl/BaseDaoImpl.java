package com.help_desk_app.dao.impl;

import com.help_desk_app.dao.BaseDao;
import com.help_desk_app.exception.NotFoundException;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Optional;

@Repository
public abstract class BaseDaoImpl<T> implements BaseDao<T> {
    private static final String GET_ALL = "from ";

    private Logger logger = Logger.getLogger(this.getClass());

    private final SessionFactory sessionFactory;
    private Class<T> aClass;

    public BaseDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
        this.aClass = (Class<T>) ((ParameterizedType)this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    public Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public Optional<T> getOne(Long id) {
        logger.debug(String.format("Try to find %s by id = %s", aClass, id));
        Optional<T> t = Optional.ofNullable(getSession().get(aClass, id));
        logger.info(String.format("%s were found %s", aClass, t));
        return t;
    }

    @Override
    public T create(T t) {
        logger.debug(String.format("Try to create %s from dto %s", aClass, t));
        T newT = (T) getSession().save(t);
        logger.info(String.format("%s was created %s", aClass, t));
        return t;
    }

    @Override
    public void update(T t) {
        logger.debug(String.format("Try to update %s from dto %s", aClass, t));
        getSession().saveOrUpdate(t);
        logger.info(String.format("%s was updated", aClass));
    }

    @Override
    public void delete(Long id) {
        logger.debug(String.format("Try to delete %s with id %s", aClass, id));
        T t = getOne(id).orElseThrow(() -> new NotFoundException(aClass, id));
        getSession().delete(t);
        logger.info(String.format("%s was deleted", aClass));

    }

    @Override
    public List<T> getAll() {
        logger.debug(String.format("Try to get all %s", aClass));
        List<T> result = getSession().createQuery((GET_ALL + aClass.getName()), aClass).list();
        logger.debug(String.format("All %s were found", aClass));
        return result;
    }
}

