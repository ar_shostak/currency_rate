package com.help_desk_app.dao;

import com.help_desk_app.entity.Currency;

import java.util.Optional;

public interface CurrencyDao extends BaseDao<Currency> {
    Optional<Currency> getOneByCurrencyId(Long id);
}
