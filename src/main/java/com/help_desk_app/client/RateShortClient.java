package com.help_desk_app.client;

import com.help_desk_app.pojo.RateShort;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Component
public class RateShortClient {
    private static final String PATTERN = "yyyy-MM-dd";
    private static final String GET_RATE_DYNAMIC_URL =
            "https://www.nbrb.by/api/exrates/rates/dynamics/{id}?startDate={start}&endDate={end}";

    private final RestTemplate restTemplate;

    public RateShortClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public List<RateShort> getById(Long id, Date startDate, Date enddate){
        SimpleDateFormat formatter = new SimpleDateFormat(PATTERN);
        List<RateShort> rateShorts = Arrays.asList(restTemplate.getForObject(GET_RATE_DYNAMIC_URL, RateShort[].class,
                id, formatter.format(startDate), formatter.format(enddate)));
        return rateShorts;
        }
}
