package com.help_desk_app.client;

import com.help_desk_app.entity.Currency;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import java.util.Arrays;
import java.util.List;

@Component
public class CurrencyClient {
    private static final String GET_CURRENCIES_ENDPOINT_URL = "https://www.nbrb.by/api/exrates/currencies";

    private final RestTemplate restTemplate;

    public CurrencyClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public List<Currency> getCurrencies() {
        return Arrays.asList(restTemplate.getForObject(GET_CURRENCIES_ENDPOINT_URL, Currency[].class));
    }
}

