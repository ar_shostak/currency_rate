## To run

  - Run next command in command prompt in directory with pom file:
```bash
mvn tomcat7:run -f pom.xml
```
   - Change directory in command prompt to 
```bash
/target/main/webapp/WEB-INF/view/react
```
 - Run command 
```bash
npm start
```
 - Now Application available on http://localhost:3000
 - BackEnd Server runs on http://localhost:8080/Help_Desk
 - to enter the application, you can use:
 username: +@.+
 password: P@ssword1 
 